
CREATE TABLE persona(
	idPersona int(11)not null auto_increment ,
	nombre VARCHAR(10) ,
    edad int (11) ,
	peso int (11) ,
	historia VARCHAR(25),
    PRIMARY KEY(idPersona)
);
create table peliSerie(
    idPeli int(11) NOT null AUTO_INCREMENT PRIMARY key,
	imagen varchar(1000) ,
	titulo varchar (20) ,
	fecha date, 
	calificación int (10)
);
create table genero(
    idgenero int(11) NOT null AUTO_INCREMENT PRIMARY key,
    nombre varchar (20) ,
	imagen varchar(1000),
    FKPeliSerie int (11) not null,
    FOREIGN KEY (FKPeliSerie) REFERENCES peliSerie(idPeli)
);
CREATE TABLE cine (
	idcine int(11) NOT null AUTO_INCREMENT PRIMARY key,
	FKPersona int (11) NOT null,
	FKPeliSerie int (11) not null,
    FOREIGN KEY (FKPeliSerie) REFERENCES peliSerie(idPeli),
    FOREIGN KEY (FKPeliSerie) REFERENCES peliSerie(idPeli)
);
CREATE TABLE users(
	idUser int(11)not null auto_increment PRIMARY KEY ,
	email VARCHAR(100) ,
    password VARCHAR(100) ,
	FKCine int (11) not null,
    FOREIGN KEY (FKCine) REFERENCES cine(idcine)
);
ALTER TABLE cine ADD FOREIGN KEY (FKPeliSerie) REFERENCES peliSerie(idPeli);
ALTER TABLE cine ADD FOREIGN KEY (FKPersona) REFERENCES persona(idPersona);

INSERT INTO persona VALUES 
(null,"Giovanny",24,70,"Developer"),
(null,"Jhony",21,65,"developer MERN"),
(null,"Yimii",24,70,"developer full stack"),
(null,"shamy",23,50,"formacion en UX XI"),
(null,"Rhonny",20,60,"Arquitectura"),
(null,"Brandon",18,65,"secundario"),
(null,"kevin",5,25,"primaria");

INSERT INTO peliSerie VALUES 
(null,"https://statics-uestudio.uecdn.es/buhomag/2019/08/Marvel-peor-reparto-posible.jpg","avengers",NOW(),1),
(null,"https://i.ytimg.com/vi/AtEasxc0v64/maxresdefault.jpg","La Bella y la Bestia",NOW(),3),
(null,"https://i.blogs.es/fc2240/los-increibles-2-pixar-trailer-final/1366_2000.jpg","Los increibles",NOW(),4),
(null,"https://statics-uestudio.uecdn.es/buhomag/2019/06/aladdin-accion-real-disney.jpg","Aladdin",NOW(),3),
(null,"https://cmonmurcia.com/wp-content/uploads/2017/12/Avatar.jpg","Avatar",NOW(),3),
(null,"https://vader.news/__export/1625534743489/sites/gadgets/img/2021/07/05/harry_potter.jpg_1383994752.jpg",
"Harry Potter:",NOW(),2);

INSERT INTO cine VALUES 
(null,1,1),(null,1,6),
(null,2,2),
(null,3,3),(null,3,4),(null,3,1),
(null,4,1),(null,4,4),(null,4,2),
(null,5,5),(null,5,6),
(null,6,6),(null,6,1),(null,6,2),
(null,7,5),(null,7,1),(null,7,4);

INSERT INTO genero VALUES
(null,"Accion","https://image.shutterstock.com/image-vector/action-movie-genre-icon-entertainment-600w-1689497347.jpg",1),
(null,"Romance","https://encrypted-tbn0.gstatic.com/images?q=tbn:ANd9GcR9u6U1gde67ZbnzeT0p8OuDdIrX38KQLUvMQ&usqp=CAU",2),
(null,"Accion","https://image.shutterstock.com/image-vector/action-movie-genre-icon-entertainment-600w-1689497347.jpg",3),
(null,"Romance","https://encrypted-tbn0.gstatic.com/images?q=tbn:ANd9GcR9u6U1gde67ZbnzeT0p8OuDdIrX38KQLUvMQ&usqp=CAU",4),
(null,"Ciencia ficcion","https://previews.123rf.com/images/sonulkaster/sonulkaster1710/sonulkaster171000210/87661176-icono-de-g%C3%A9nero-de-pel%C3%ADcula-logo-de-ciencia-ficci%C3%B3n-del-cohete-espacial-y-globo-del-mundo-vector-plano-aisl.jpg",5),
(null,"Ciencia ficcion","https://previews.123rf.com/images/sonulkaster/sonulkaster1710/sonulkaster171000210/87661176-icono-de-g%C3%A9nero-de-pel%C3%ADcula-logo-de-ciencia-ficci%C3%B3n-del-cohete-espacial-y-globo-del-mundo-vector-plano-aisl.jpg",6);

/* 
referencia: 
http://sql.11sql.com/sql-foreign-key.htm
https://www.srcodigofuente.es/sql-insert-date-now 
*/
const bcrypt = require('bcryptjs');

class IncriptPass{
   
    constructor(){
        
    }
    async encryptPassword(password:string):Promise<string>{
     
        const salt = await bcrypt.genSalt(10);
        const hash = await bcrypt.hash(password, salt);
        return hash;
     
        
    };
    async matchPassword (password:string, BBDDPassword:string)  {
        try {
          return await bcrypt.compare(password, BBDDPassword);
        } catch (e) {
          console.log(e)
        }
    };
}
const incriptPass=new IncriptPass()
export default incriptPass


import { json, Request, Response } from 'express'
import passport from "passport";
const { isLoggedIn } = require('../lib/auth');
import { Router } from 'express'

class Authentication {
    public router: Router = Router()
    constructor() {
        this.signup()
    }
    signup(): void {
        // SIGNUP
        this.router.get('/signup', (req: Request, res: Response) => {
            res.render('auth/signup');
        });
        this.router.post('/signup', passport.authenticate('local.signup', {
            successRedirect: '/admin',
            failureRedirect: '/login',
            failureFlash: true
        }));

        // SINGIN
        this.router.get('/signin', (req, res) => {
            res.render('auth/signin');
        });

        this.router.post('/signin', (req, res, next) => {
            /* req.check('username', 'Username is Required').notEmpty();
            req.check('password', 'Password is Required').notEmpty();
            const errors = req.validationErrors();
            if (errors.length > 0) {
                req.flash('message', errors[0].msg);
                res.redirect('/signin');
            }
            passport.authenticate('local.signin', {
                successRedirect: '/profile',
                failureRedirect: '/signin',
                failureFlash: true
            })(req, res, next); */
        });

        this.router.get('/logout', (req, res) => {
            req.logOut();
            res.redirect('/');
        });

        this.router.get('/profile', isLoggedIn, (req, res) => {
            res.render('profile');
        });
    }
}
const authentication = new Authentication()
export default authentication.router


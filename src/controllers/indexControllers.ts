import {Request, Response} from 'express'
import pool from '../database'
import { Persona } from '../interfaces/persona';
import  {personaControllers}from "./PersControllers"
class IndexControllers{
    public async index (req: Request, res: Response) {
        try {
           // console.log(JuegoBBDD)
           await res.render("index");

        } catch (error) {
            res.render("404");
        }

    }
    public async admin (req: Request, res: Response) {
        try {
           // console.log(JuegoBBDD)
           await pool.query('SELECT * FROM persona', (err, result, fields)=> {
                if (err) throw err;
                console.log("respuesta: ",(result));
                res.render("admin",{"admin":result});
            });
        } catch (error) {
            res.render("404");
        }

    }
}
export const indexControllers =new IndexControllers()
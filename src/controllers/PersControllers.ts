import {json, Request, Response} from 'express'
import pool from '../database'
import { Persona } from '../interfaces/persona';


class PersonaControllers{
    public async getRegiter (req: Request, res: Response) {
        try {
           // console.log(JuegoBBDD)
           await res.render("formulario");
        } catch (error) {
            res.render("404");
        }
    }
    public async getAll_lists (req: Request, res: Response):Promise<void> {
        await pool.query('SELECT * FROM persona', (err, result, fields)=> {
            if (err) throw err;
            res.json(result);
        });
    }
    public async getl_id (req:Request,res:Response):Promise<any>{
        //res.send('games')
        /* pool.query('DESCRIBE games')
        res.json( `opteniiendo el juegos N° ${req.params.id} `)) */
        const {id}=req.params;
        await pool.query('SELECT * FROM persona WHERE idPersona = ?', [id], (err, result, fields)=> {
            if (err) throw err;
            //console.log(req.params) //{ id: '1' }
            if(result.length>0){
                console.log(`opteniiendo el juegos N° ${req.params.id} `)
                console.log(result)
                res.status(200).json(result);
            }else{
                console.log('resultado NOOOO encontrado')
                res.status(404).json('resultado NOOOO encontrado');
            }

        });
    }
    public async create(req:Request,res:Response):Promise<void>{
         const form:Persona={
            nombre:req.body.nombre,
            edad:parseInt(req.body.edad), 
            peso:parseInt(req.body.peso), 
            historia:req.body.historia
        } 
        //const forma:Persona= await this.bodyPerson(req.body)
        console.log("create: ",form)
        try {
            await pool.query('INSERT INTO persona SET ?', [form], (err) => {
                if (err) throw err;
                console.log('perosnaje gurdado: ',form)
                res.json({message:'Personaje guardado..!'})
                //res.redirect('/')
            })
        } catch (error) {
            res.status(404).json({message:'No se guardo el personaje..!'})
        }        
    }
    public async delete(req:Request,res:Response):Promise<void>{
        const {id}=req.params;
        try {
            await pool.query('DELETE FROM persona WHERE idPersona = ?', [id], (err, result)=> {
                if (err) throw err;
                console.log(`Eliminado el personaje N° ${req.params.id} `)
                res.json(`Eliminado el Personaje N° ${req.params.id}, Exito!! `);         
            });
        } catch (error) {
            res.status(404).json({message:'No se Elimino el personaje..!'})
        }
       
    }
    public async update(req:Request,res:Response):Promise<void>{
        //res.json({text:'actualizando un juego '+req.params.id})
        const {id}=req.params;
        console.log("params: ",req.params,"body: ")
        try {
            await pool.query('UPDATE persona SET ? WHERE idPersona = ?', [req.body,id], (err, result)=> {
                if (err) throw err;
                console.log(`El personaje  N° ${req.params.id} fue ACTUALIZADO `)
                res.json(`El personaje  N° ${req.params.id} fue ACTUALIZADO con Exito!! `);         
            });
        } catch (error) {
            res.status(404).json({message:'No se Actualizo el personaje..!'})
        }
        
    }
    public async detallePersonaje(req:Request,res:Response):Promise<void>{
        
    }

    /* public bodyPerson(body:Request):Persona{
         const form:Persona={
            nombre:req.body.nombre,
            edad:parseInt(req.body.edad), 
            peso:parseInt(req.body.peso), 
            historia:req.body.historia
        }
        return form;
    } */
}
export const personaControllers =new PersonaControllers()
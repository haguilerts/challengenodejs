import express, {Application} from 'express';
import morgan from 'morgan';
import cors from 'cors'
import personaRouter from './routers/persRouter';
import authRouter from './routers/authRouter';
import indexRouters from './routers/indexRouters';
import {initialize, session} from 'passport'

class Server{
    public app:Application
    constructor(){
        this.app=express()
        this.config()
        this.router()
        require('./config/passport')
    }
    config():void{
        this.app.set('port', process.env.PORT || 3200)
        this.app.set("view engine", "ejs");
        this.app.set("views", __dirname + "/views");
        this.app.use(morgan('dev'))
        this.app.use(cors())
        this.app.use(express.json())
        this.app.use(express.urlencoded({extended:false}))
        this.app.use(initialize());
        this.app.use(session());
    }
    router():void{
        this.app.use('/',indexRouters)
        this.app.use('/characters',personaRouter)
        this.app.use('/auth',authRouter)
        //this.app.use('/login',)
        
    }
    start():void{
        this.app.listen(this.app.get('port'),()=>{
            console.log(`Server on port ${this.app.get('port')} : http://localhost:3200/`)
        });
    }
}
const server=new Server();
server.start(); 
import mysql from 'mysql'
import keys from './keys'
import { promisify } from 'util'
const pool= mysql.createPool(keys.database) // ejecuta la concexion a la BBDd

pool.getConnection((err, connection) => {
    if (err) throw err;
    try {
        connection.release();
        console.log('DB is connected exit 😃');
    } catch (err) {
        console.log('DB not is connected  😩..');
        
    }
   
});
//promysify pool query
//pool.query = promisify(pool.query);
export default pool;


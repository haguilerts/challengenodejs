import {Router}from 'express'
import {personaControllers} from '../controllers/PersControllers'

class PersonaRouter{
    public router:Router=Router()
    constructor(){
        this.config()
    }
    config():void{
       this.router.get('/',personaControllers.getAll_lists)
       this.router.get('/:id',personaControllers.getl_id)
       this.router.post('/', personaControllers.create)
       this.router.delete('/:id', personaControllers.delete)
       this.router.put('/:id', personaControllers.update)
    }
}
const personaRouter=new PersonaRouter()
export default personaRouter.router
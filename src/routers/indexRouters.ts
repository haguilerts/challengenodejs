import {Router}from 'express'
import {indexControllers} from '../controllers/indexControllers'
class IndexRouter{
    public router:Router=Router()
    constructor(){
        this.config()
    }
    config():void{
       this.router.get('/', indexControllers.index)
       this.router.get('/admin', indexControllers.admin)
    }
}
//(req,res)=> res.send('hello')
const indexRouter=new IndexRouter()
export default indexRouter.router
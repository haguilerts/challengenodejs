import {Router}from 'express';
import {loginControllers} from '../controllers/loginControllers';
import { personaControllers } from '../controllers/PersControllers';
import passport from "passport";
class AuthRouter{
    public router:Router=Router()
    constructor(){
        this.config()
    }
    config():void{
        this.router.get('/login', loginControllers.getLogin );
        this.router.post('/login',passport.authenticate('local.signup', {
            successRedirect: '/admin',
            failureRedirect: '/login',
            failureFlash: true
        }),loginControllers.postLogin ); 
          
        this.router.get('/register', personaControllers.getRegiter);
        this.router.post('/register', personaControllers.create);
        this.router.post('/admin', );

    }
}
const authRouter=new AuthRouter()
export default authRouter.router
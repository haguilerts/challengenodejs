export interface Persona{
    id?:number,
    nombre:string,
    edad:number,
    peso:number,
    historia:string
}
export interface Login{
    id?:number,
    email:string,
    password:string
}
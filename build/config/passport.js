"use strict";
var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    function adopt(value) { return value instanceof P ? value : new P(function (resolve) { resolve(value); }); }
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : adopt(result.value).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
//const passport = require('passport');
const LocalStrategy = require('passport-local').Strategy;
const passport_1 = __importDefault(require("passport"));
const database_1 = __importDefault(require("../database"));
const incriPass_1 = __importDefault(require("./incriPass"));
passport_1.default.use('local.signup', new LocalStrategy({
    usernamefield: 'email',
    password: 'password',
    passReqToCallback: true
}, (req, email, password, done) => __awaiter(void 0, void 0, void 0, function* () {
    console.log('req.body:', req.body);
    const newuser = {
        email,
        password,
    };
    newuser.password = yield incriPass_1.default.encryptPassword(password);
    const res = yield database_1.default.query('INSERT INTO users SET ?', [newuser]);
    console.log("res: ", res);
    //newuser.id= res.insertId;
    return done(null, newuser);
})));
passport_1.default.serializeUser((user, done) => {
    console.log("user: ", user);
    //done(null, user.id)
});
passport_1.default.deserializeUser((id, done) => __awaiter(void 0, void 0, void 0, function* () {
    const rows = yield database_1.default.query('SELECT * FROM persona WHERE idPersona = ?', [id]);
    //done(null, rows[0])
    console.log("rows: ", rows);
}));

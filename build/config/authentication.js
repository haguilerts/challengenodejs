"use strict";
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
const passport_1 = __importDefault(require("passport"));
const { isLoggedIn } = require('../lib/auth');
const express_1 = require("express");
class Authentication {
    constructor() {
        this.router = express_1.Router();
        this.signup();
    }
    signup() {
        // SIGNUP
        this.router.get('/signup', (req, res) => {
            res.render('auth/signup');
        });
        this.router.post('/signup', passport_1.default.authenticate('local.signup', {
            successRedirect: '/admin',
            failureRedirect: '/login',
            failureFlash: true
        }));
        // SINGIN
        this.router.get('/signin', (req, res) => {
            res.render('auth/signin');
        });
        this.router.post('/signin', (req, res, next) => {
            /* req.check('username', 'Username is Required').notEmpty();
            req.check('password', 'Password is Required').notEmpty();
            const errors = req.validationErrors();
            if (errors.length > 0) {
                req.flash('message', errors[0].msg);
                res.redirect('/signin');
            }
            passport.authenticate('local.signin', {
                successRedirect: '/profile',
                failureRedirect: '/signin',
                failureFlash: true
            })(req, res, next); */
        });
        this.router.get('/logout', (req, res) => {
            req.logOut();
            res.redirect('/');
        });
        this.router.get('/profile', isLoggedIn, (req, res) => {
            res.render('profile');
        });
    }
}
const authentication = new Authentication();
exports.default = authentication.router;

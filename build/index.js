"use strict";
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
const express_1 = __importDefault(require("express"));
const morgan_1 = __importDefault(require("morgan"));
const cors_1 = __importDefault(require("cors"));
const persRouter_1 = __importDefault(require("./routers/persRouter"));
const authRouter_1 = __importDefault(require("./routers/authRouter"));
const indexRouters_1 = __importDefault(require("./routers/indexRouters"));
const passport_1 = require("passport");
class Server {
    constructor() {
        this.app = express_1.default();
        this.config();
        this.router();
        require('./config/passport');
    }
    config() {
        this.app.set('port', process.env.PORT || 3200);
        this.app.set("view engine", "ejs");
        this.app.set("views", __dirname + "/views");
        this.app.use(morgan_1.default('dev'));
        this.app.use(cors_1.default());
        this.app.use(express_1.default.json());
        this.app.use(express_1.default.urlencoded({ extended: false }));
        this.app.use(passport_1.initialize());
        this.app.use(passport_1.session());
    }
    router() {
        this.app.use('/', indexRouters_1.default);
        this.app.use('/characters', persRouter_1.default);
        this.app.use('/auth', authRouter_1.default);
        //this.app.use('/login',)
    }
    start() {
        this.app.listen(this.app.get('port'), () => {
            console.log(`Server on port ${this.app.get('port')} : http://localhost:3200/`);
        });
    }
}
const server = new Server();
server.start();

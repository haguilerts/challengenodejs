"use strict";
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
const mysql_1 = __importDefault(require("mysql"));
const keys_1 = __importDefault(require("./keys"));
const pool = mysql_1.default.createPool(keys_1.default.database); // ejecuta la concexion a la BBDd
pool.getConnection((err, connection) => {
    if (err)
        throw err;
    try {
        connection.release();
        console.log('DB is connected exit 😃');
    }
    catch (err) {
        console.log('DB not is connected  😩..');
    }
});
//promysify pool query
//pool.query = promisify(pool.query);
exports.default = pool;

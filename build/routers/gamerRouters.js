"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
const express_1 = require("express");
const gameControlers_1 = require("../controllers/gameControlers");
class GamesRouter {
    constructor() {
        this.router = express_1.Router();
        this.config();
    }
    config() {
        this.router.get('/', gameControlers_1.gameControllers.getAll_lists);
        this.router.get('/:id', gameControlers_1.gameControllers.getlist);
        this.router.post('/', gameControlers_1.gameControllers.create);
        this.router.delete('/:id', gameControlers_1.gameControllers.delete);
        this.router.put('/:id', gameControlers_1.gameControllers.update);
    }
}
const gamesRouter = new GamesRouter();
exports.default = gamesRouter.router;

"use strict";
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
const express_1 = require("express");
const loginControllers_1 = require("../controllers/loginControllers");
const PersControllers_1 = require("../controllers/PersControllers");
const passport_1 = __importDefault(require("passport"));
class AuthRouter {
    constructor() {
        this.router = express_1.Router();
        this.config();
    }
    config() {
        this.router.get('/login', loginControllers_1.loginControllers.getLogin);
        this.router.post('/login', passport_1.default.authenticate('local.signup', {
            successRedirect: '/admin',
            failureRedirect: '/login',
            failureFlash: true
        }), loginControllers_1.loginControllers.postLogin);
        this.router.get('/register', PersControllers_1.personaControllers.getRegiter);
        this.router.post('/register', PersControllers_1.personaControllers.create);
        this.router.post('/admin');
    }
}
const authRouter = new AuthRouter();
exports.default = authRouter.router;

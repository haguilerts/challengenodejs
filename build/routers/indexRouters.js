"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
const express_1 = require("express");
const indexControllers_1 = require("../controllers/indexControllers");
class IndexRouter {
    constructor() {
        this.router = express_1.Router();
        this.config();
    }
    config() {
        this.router.get('/', indexControllers_1.indexControllers.index);
        this.router.get('/admin', indexControllers_1.indexControllers.admin);
    }
}
//(req,res)=> res.send('hello')
const indexRouter = new IndexRouter();
exports.default = indexRouter.router;

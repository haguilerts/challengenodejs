"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
const express_1 = require("express");
const PersControllers_1 = require("../controllers/PersControllers");
class PersonaRouter {
    constructor() {
        this.router = express_1.Router();
        this.config();
    }
    config() {
        this.router.get('/', PersControllers_1.personaControllers.getAll_lists);
        this.router.get('/:id', PersControllers_1.personaControllers.getl_id);
        this.router.post('/', PersControllers_1.personaControllers.create);
        this.router.delete('/:id', PersControllers_1.personaControllers.delete);
        this.router.put('/:id', PersControllers_1.personaControllers.update);
    }
}
const personaRouter = new PersonaRouter();
exports.default = personaRouter.router;

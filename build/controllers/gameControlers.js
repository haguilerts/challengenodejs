"use strict";
var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    function adopt(value) { return value instanceof P ? value : new P(function (resolve) { resolve(value); }); }
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : adopt(result.value).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
const database_1 = __importDefault(require("../database"));
class GamesControllers {
    /* public async  (req:Request,res:Response){
        //res.send('games')
        const juego = await pool.query('SELECT * FROM games')
        res.json(juego)
    } */
    getAll_lists(req, res) {
        return __awaiter(this, void 0, void 0, function* () {
            yield database_1.default.query('SELECT * FROM persona', (err, result, fields) => {
                if (err)
                    throw err;
                res.json(result);
            });
        });
    }
    getlist(req, res) {
        return __awaiter(this, void 0, void 0, function* () {
            //res.send('games')
            /* pool.query('DESCRIBE games')
            res.json( `opteniiendo el juegos N° ${req.params.id} `)) */
            const { id } = req.params;
            yield database_1.default.query('SELECT * FROM persona WHERE id = ?', [id], (err, result, fields) => {
                if (err)
                    throw err;
                //console.log(req.params) //{ id: '1' }
                if (result.length > 0) {
                    console.log(`opteniiendo el juegos N° ${req.params.id} `);
                    console.log(result);
                    res.status(200).json(result);
                }
                else {
                    console.log('resultado NOOOO encontrado');
                    res.status(404).json('resultado NOOOO encontrado');
                }
            });
        });
    }
    create(req, res) {
        return __awaiter(this, void 0, void 0, function* () {
            yield database_1.default.query('INSERT INTO games SET ?', [req.body], (err) => {
                if (err)
                    throw err;
                res.json({ message: 'juego guardado..!' });
                //res.redirect('/')
            });
        });
    }
    delete(req, res) {
        return __awaiter(this, void 0, void 0, function* () {
            const { id } = req.params;
            yield database_1.default.query('DELETE FROM persona WHERE id = ?', [id], (err, result) => {
                if (err)
                    throw err;
                console.log(`Eliminado el juegos N° ${req.params.id} `);
                res.json(`Eliminado el juegos N° ${req.params.id}, Exito!! `);
            });
        });
    }
    update(req, res) {
        return __awaiter(this, void 0, void 0, function* () {
            //res.json({text:'actualizando un juego '+req.params.id})
            const { id } = req.params;
            yield database_1.default.query('UPDATE persona SET ? WHERE id = ?', [req.body, id], (err, result) => {
                if (err)
                    throw err;
                console.log(`El juegos  N° ${req.params.id} fue ACTUALIZADO `);
                res.json(`El juegos  N° ${req.params.id} fue ACTUALIZADO con Exito!! `);
            });
        });
    }
}
exports.gameControllers = new GamesControllers();

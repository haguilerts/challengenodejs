"use strict";
var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    function adopt(value) { return value instanceof P ? value : new P(function (resolve) { resolve(value); }); }
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : adopt(result.value).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
const database_1 = __importDefault(require("../database"));
class LoginControllers {
    getLogin(req, res) {
        return __awaiter(this, void 0, void 0, function* () {
            try {
                // console.log(JuegoBBDD)
                yield res.render("login");
            }
            catch (error) {
                res.render("404");
            }
        });
    }
    getRegiter(req, res) {
        return __awaiter(this, void 0, void 0, function* () {
            try {
                // console.log(JuegoBBDD)
                yield res.render("formulario");
            }
            catch (error) {
                res.render("404");
            }
        });
    }
    postLogin(req, res) {
        return __awaiter(this, void 0, void 0, function* () {
            try {
                const mybody = {
                    email: req.body.email,
                    password: req.body.email
                };
                console.log("cuerpo: ", req.body);
                console.log('mybody: ', mybody);
                yield database_1.default.query('SELECT * FROM `users` WHERE email=? and password=?', [mybody.email, mybody.password], (err, result, fields) => {
                    if (err)
                        throw err;
                    //console.log(req.params) //{ id: '1' }
                    if (result.length > 0) {
                        console.log(`Resultado encontrado: `, result);
                        //console.log(result)
                        res.status(200).render("index");
                    }
                    else {
                        console.log('resultado NOOOO encontrado');
                        res.status(404).json('resultado NOOOO encontrado');
                    }
                });
            }
            catch (error) {
                console.log("ususario incorrecto!!");
                res.status(404).render("login");
            }
        });
    }
}
exports.loginControllers = new LoginControllers();

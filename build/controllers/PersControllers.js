"use strict";
var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    function adopt(value) { return value instanceof P ? value : new P(function (resolve) { resolve(value); }); }
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : adopt(result.value).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
const database_1 = __importDefault(require("../database"));
class PersonaControllers {
    getRegiter(req, res) {
        return __awaiter(this, void 0, void 0, function* () {
            try {
                // console.log(JuegoBBDD)
                yield res.render("formulario");
            }
            catch (error) {
                res.render("404");
            }
        });
    }
    getAll_lists(req, res) {
        return __awaiter(this, void 0, void 0, function* () {
            yield database_1.default.query('SELECT * FROM persona', (err, result, fields) => {
                if (err)
                    throw err;
                res.json(result);
            });
        });
    }
    getl_id(req, res) {
        return __awaiter(this, void 0, void 0, function* () {
            //res.send('games')
            /* pool.query('DESCRIBE games')
            res.json( `opteniiendo el juegos N° ${req.params.id} `)) */
            const { id } = req.params;
            yield database_1.default.query('SELECT * FROM persona WHERE idPersona = ?', [id], (err, result, fields) => {
                if (err)
                    throw err;
                //console.log(req.params) //{ id: '1' }
                if (result.length > 0) {
                    console.log(`opteniiendo el juegos N° ${req.params.id} `);
                    console.log(result);
                    res.status(200).json(result);
                }
                else {
                    console.log('resultado NOOOO encontrado');
                    res.status(404).json('resultado NOOOO encontrado');
                }
            });
        });
    }
    create(req, res) {
        return __awaiter(this, void 0, void 0, function* () {
            const form = {
                nombre: req.body.nombre,
                edad: parseInt(req.body.edad),
                peso: parseInt(req.body.peso),
                historia: req.body.historia
            };
            //const forma:Persona= await this.bodyPerson(req.body)
            console.log("create: ", form);
            try {
                yield database_1.default.query('INSERT INTO persona SET ?', [form], (err) => {
                    if (err)
                        throw err;
                    console.log('perosnaje gurdado: ', form);
                    res.json({ message: 'Personaje guardado..!' });
                    //res.redirect('/')
                });
            }
            catch (error) {
                res.status(404).json({ message: 'No se guardo el personaje..!' });
            }
        });
    }
    delete(req, res) {
        return __awaiter(this, void 0, void 0, function* () {
            const { id } = req.params;
            try {
                yield database_1.default.query('DELETE FROM persona WHERE idPersona = ?', [id], (err, result) => {
                    if (err)
                        throw err;
                    console.log(`Eliminado el personaje N° ${req.params.id} `);
                    res.json(`Eliminado el Personaje N° ${req.params.id}, Exito!! `);
                });
            }
            catch (error) {
                res.status(404).json({ message: 'No se Elimino el personaje..!' });
            }
        });
    }
    update(req, res) {
        return __awaiter(this, void 0, void 0, function* () {
            //res.json({text:'actualizando un juego '+req.params.id})
            const { id } = req.params;
            console.log("params: ", req.params, "body: ");
            try {
                yield database_1.default.query('UPDATE persona SET ? WHERE idPersona = ?', [req.body, id], (err, result) => {
                    if (err)
                        throw err;
                    console.log(`El personaje  N° ${req.params.id} fue ACTUALIZADO `);
                    res.json(`El personaje  N° ${req.params.id} fue ACTUALIZADO con Exito!! `);
                });
            }
            catch (error) {
                res.status(404).json({ message: 'No se Actualizo el personaje..!' });
            }
        });
    }
    detallePersonaje(req, res) {
        return __awaiter(this, void 0, void 0, function* () {
        });
    }
}
exports.personaControllers = new PersonaControllers();
